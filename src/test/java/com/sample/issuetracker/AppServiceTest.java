package com.sample.issuetracker;

import com.sample.issuetracker.action.ActionExecutor;
import com.sample.issuetracker.model.entities.Issue;
import com.sample.issuetracker.model.entities.Transition;
import com.sample.issuetracker.model.repositories.IssueRepository;
import com.sample.issuetracker.model.repositories.TransitionRepository;
import com.sample.issuetracker.model.repositories.UserRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class AppServiceTest {

    @InjectMocks
    private AppService service;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private IssueRepository issueRepository;

    @MockBean
    private TransitionRepository transitionRepository;

    @MockBean
    private ActionExecutor actionExecutor;


    final List<Transition> workflow = new ArrayList<>(Arrays.asList(
            new Transition("Open", "In Progress", "Start Progress"),
            new Transition("In Progress", "Resolved", "Resolve Issue"),
            new Transition("Resolved", null, null)
    ));

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_saveIssue_identical() {
        final Issue issue = new Issue(123, "Open", 101);

        Mockito.when(issueRepository.findById(123)).thenReturn(Optional.of(new Issue(123, "Open", 101)));
        Mockito.when(transitionRepository.findAll()).thenReturn(workflow);
        Mockito.when(issueRepository.save(issue)).thenReturn(issue);

        assertEquals(issue, service.saveIssue(issue));
    }

    @Test
    public void test_saveIssue_execute_transition() {
        final Issue issue = new Issue(123, "In Progress", 101);

        Mockito.when(issueRepository.findById(123)).thenReturn(Optional.of(new Issue(123, "Open", 101)));
        Mockito.when(transitionRepository.findAll()).thenReturn(workflow);
        Mockito.when(issueRepository.save(issue)).thenReturn(issue);

        assertEquals(issue, service.saveIssue(issue));
    }

    @Test
    public void test_saveIssue_incorrect_transition() throws Exception {
        final Issue issue = new Issue(123, "Resolved", 101);

        Mockito.when(issueRepository.findById(123)).thenReturn(Optional.of(new Issue(123, "Open", 101)));
        Mockito.when(transitionRepository.findAll()).thenReturn(workflow);
        Mockito.when(issueRepository.save(issue)).thenReturn(issue);

        expectedEx.expect(ResponseStatusException.class);
        expectedEx.expectMessage("Issue transition from Open to Resolved is not allowed by current Workflow");

        service.saveIssue(issue);
    }

    @Test
    public void test_saveIssue_non_existing_transition() throws Exception {
        final Issue issue = new Issue(123, "Updated", 101);

        Mockito.when(issueRepository.findById(123)).thenReturn(Optional.of(new Issue(123, "Open", 101)));
        Mockito.when(transitionRepository.findAll()).thenReturn(workflow);
        Mockito.when(issueRepository.save(issue)).thenReturn(issue);

        expectedEx.expect(ResponseStatusException.class);
        expectedEx.expectMessage("Issue transition from Open to Updated is not allowed by current Workflow");

        service.saveIssue(issue);
    }

    @Test
    public void test_saveIssue_new_issue() {
        final Issue issue = new Issue(100, "Open", 101);
        final Issue savedIssue = new Issue(124, "Open", 101);

        Mockito.when(issueRepository.findById(100)).thenReturn(Optional.empty());
        Mockito.when(transitionRepository.findAll()).thenReturn(workflow);
        Mockito.when(issueRepository.save(issue)).thenReturn(savedIssue);

        assertEquals(savedIssue, service.saveIssue(issue));
    }

    @Test
    public void test_saveIssue_new_issue_with_incorrect_state() throws Exception {
        final Issue issue = new Issue(123, "Updated", 101);

        Mockito.when(issueRepository.findById(100)).thenReturn(Optional.empty());
        Mockito.when(transitionRepository.findAll()).thenReturn(workflow);
        Mockito.when(issueRepository.save(issue)).thenReturn(issue);

        expectedEx.expect(ResponseStatusException.class);
        expectedEx.expectMessage("New Issue cannot be created with non existing status: Updated");

        service.saveIssue(issue);
    }
}