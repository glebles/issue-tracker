package com.sample.issuetracker;

import com.sample.issuetracker.model.entities.User;
import com.sample.issuetracker.model.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AppControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository userRepository;


    @Test
    public void test_getAllUsers() throws Exception {
        final Iterable<User> users = Arrays.asList(
                new User(1, "User1"),
                new User(2, "User2"));

        Mockito.when(userRepository.findAll()).thenReturn(users);

        this.mockMvc
                .perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].name").value("User1"))
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].name").value("User2"));
    }

	@Test
	public void test_saveUser() throws Exception {
		final User user = new User(1, "User1");

		Mockito.when(userRepository.save(user)).thenReturn(user);

		this.mockMvc
				.perform(post("/users")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{\"id\":\"1\",\"name\":\"User1\"}"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value("1"))
				.andExpect(jsonPath("$.name").value("User1"));
	}

    @Test
    public void test_deleteUser() throws Exception {


        this.mockMvc
                .perform(delete("/users/{id}", "1"))
                .andExpect(status().isOk());

        verify(userRepository, times(1)).deleteById(1);
    }
 }
