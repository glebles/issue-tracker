package com.sample.issuetracker;

import com.sample.issuetracker.action.ActionExecutor;
import com.sample.issuetracker.action.ActionFactory;
import com.sample.issuetracker.model.entities.Issue;
import com.sample.issuetracker.model.entities.Transition;
import com.sample.issuetracker.model.entities.User;
import com.sample.issuetracker.model.repositories.IssueRepository;
import com.sample.issuetracker.model.repositories.TransitionRepository;
import com.sample.issuetracker.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class AppService {

    private static final List<Transition> defaultWorkflow = new ArrayList<>(Arrays.asList(
            new Transition("Open", "In Progress", "Start Progress"),
            new Transition("Open", "Closed", "Close Issue"),
            new Transition("Open", "Resolved", "Resolve Issue"),
            new Transition("In Progress", "Open", "Stop Issue"),
            new Transition("In Progress", "Resolved", "Resolve Issue"),
            new Transition("Reopen", "In Progress", "Start Progress"),
            new Transition("Reopen", "Closed", "Close Issue"),
            new Transition("Reopen", "Resolved", "Resolve Issue"),
            new Transition("Closed", "Reopen", "Reopen Issue"),
            new Transition("Resolved", "Closed", "Close Issue"),
            new Transition("Resolved", "Reopen", "Reopen Issue")
    ));

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private TransitionRepository transitionRepository;

    @Autowired
    private ActionExecutor actionExecutor;

    Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    User saveUser(User user) {
        return userRepository.save(user);
    }

    void deleteUser(int id) {
        userRepository.deleteById(id);
    }

    Iterable<Issue> getAllIssues() {
        return issueRepository.findAll();
    }

    Issue getIssue(int id) {
        return issueRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Issue " + id));
    }

    public Issue saveIssue(Issue issueToSave) {
        final Optional<Issue> existingIssueOpt = issueRepository.findById(issueToSave.getId());

        final String endStatus = issueToSave.getStatus();

        if (existingIssueOpt.isPresent()) {
            final String startStatus = existingIssueOpt.get().getStatus();

            if(!startStatus.equals(endStatus)) {
                if(!getAllStatusesForIssue(existingIssueOpt.get().getId()).contains(endStatus))
                    throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE,
                            "Issue transition from " + startStatus + " to " + endStatus + " is not allowed by current Workflow");
                
//                final Transition transition = getTransitionFromStatuses(startStatus, endStatus);
//                actionExecutor.executeActions(transition
//                        .getTransitionActions()
//                        .stream()
//                        .map(transitionAction -> ActionFactory.build(transition, transitionAction, issueToSave))
//                        .collect(Collectors.toList()));
            }
        } else {
            if(!getAllStatuses().contains(endStatus))
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE,
                        "New Issue cannot be created with non existing status: " + endStatus);
            issueToSave.setId(null);
        }

        return issueRepository.save(issueToSave);
    }

    private Transition getTransitionFromStatuses(String startStatus, String endStatus) {
        final Optional<Transition> ot = StreamSupport.stream(transitionRepository.findAll().spliterator(), false)
                .filter(t -> t.getStartStatus().equals(startStatus) && t.getEndStatus().equals(endStatus))
                .findAny();

        if (!ot.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE,
                    "Issue transition from " + startStatus + " to " + endStatus + " is not allowed by current Workflow");
        }

        return ot.get();
    }

    void deleteIssue(int id) {
        issueRepository.deleteById(id);
    }

    Iterable<Transition> getAllTransitions() {
        return transitionRepository.findAll();
    }

    Transition saveTransition(Transition transition) {
        return transitionRepository.save(transition);
    }

    void deleteTransition(int id) {
        transitionRepository.deleteById(id);
    }

    void initDefaultWorkflow() {
        transitionRepository.deleteAll();
        transitionRepository.saveAll(defaultWorkflow);
    }

    public List<String> getAllStatuses() {
        return StreamSupport.stream(transitionRepository.findAll().spliterator(), false)
                .map(Transition::getStartStatus)
                .distinct()
                .collect(Collectors.toList());
    }

    public List<String> getAllStatusesForIssue(int id) {
        final String startStatus = getIssue(id).getStatus();
        return StreamSupport.stream(transitionRepository.findAll().spliterator(), false)
                .filter(i->i.getStartStatus().equals(startStatus))
                .map(Transition::getEndStatus)
                .distinct()
                .collect(Collectors.toList());
    }
}
