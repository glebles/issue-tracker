package com.sample.issuetracker.action;

import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ActionExecutor {
    private final ExecutorService executor = Executors.newCachedThreadPool();

    public void  executeActions(Iterable<Action> actions) {
        actions.forEach(action -> executor.execute(action::execute));
    }
}
