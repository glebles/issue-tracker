package com.sample.issuetracker.action;

import com.sample.issuetracker.model.entities.Issue;
import com.sample.issuetracker.model.entities.Transition;
import com.sample.issuetracker.model.entities.TransitionAction;

public class EmptyAction implements Action {
    private final String message;

    public EmptyAction(Transition transition, TransitionAction transitionAction, Issue issue) {
        message = "Action Type: " + transitionAction.getType()
                + "; Start Status: " + transition.getStartStatus()
                + "; End Status: " + transition.getEndStatus()
                + "; Issue: " + issue.getId();
    }

    @Override
    public void execute() {
        System.out.println(message);
    }
}
