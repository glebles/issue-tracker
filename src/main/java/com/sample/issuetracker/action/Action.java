package com.sample.issuetracker.action;

public interface Action {
    void execute();
}
