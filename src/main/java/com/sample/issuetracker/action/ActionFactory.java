package com.sample.issuetracker.action;

import com.sample.issuetracker.model.entities.Issue;
import com.sample.issuetracker.model.entities.Transition;
import com.sample.issuetracker.model.entities.TransitionAction;


public class ActionFactory {
    public static Action build(Transition transition, TransitionAction transitionAction, Issue issue) {
        if(transitionAction.getType().equals("Email"))
            return new EmailAction(transition, issue);

        return new EmptyAction(transition, transitionAction, issue);
    }
}
