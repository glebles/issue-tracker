package com.sample.issuetracker.action;

import com.sample.issuetracker.model.entities.Issue;
import com.sample.issuetracker.model.entities.Transition;

public class EmailAction implements Action {
    private final String message;

    public EmailAction(Transition transition, Issue issue) {
        message = "Send Email for Issue: " + issue.getId()
                + " transitioning from " + transition.getStartStatus()
                + " to " + transition.getEndStatus();
    }

    @Override
    public void execute() {
        System.out.println(message);
    }
}
