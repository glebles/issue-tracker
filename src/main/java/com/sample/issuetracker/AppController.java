package com.sample.issuetracker;

import com.sample.issuetracker.model.IssueCreateDTO;
import com.sample.issuetracker.model.IssueUpdateDTO;
import com.sample.issuetracker.model.entities.Issue;
import com.sample.issuetracker.model.entities.Transition;
import com.sample.issuetracker.model.entities.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AppController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    AppService service;


//    ***********************************************************************************
//    ***********************************************************************************
//    ***********************************************************************************

    @GetMapping("/users")
    Iterable<User> getAllUsers() {
        return service.getAllUsers();
    }

    @PostMapping("/users")
    User saveUser(@RequestBody User user) {
        return service.saveUser(user);
    }

    @DeleteMapping("/users/{id}")
    void deleteUser(@PathVariable int id) {
        service.deleteUser(id);
    }

//    ***********************************************************************************
//    ***********************************************************************************
//    ***********************************************************************************

    @GetMapping("/issues")
    Iterable<Issue> getAllIssues() {
        return service.getAllIssues();
    }

    @GetMapping("/issues/{id}")
    Issue getIssue(@PathVariable int id) {
        return service.getIssue(id);
    }

    @PostMapping("/issues")
    Issue createIssue(@RequestBody Issue issue) {
        return service.saveIssue(issue);
    }

    @PutMapping("/issues")
    Issue saveIssue(@RequestBody Issue issue) {
        return service.saveIssue(issue);
    }

    @DeleteMapping("/issues/{id}")
    void deleteIssue(@PathVariable int id) {
        service.deleteIssue(id);
    }

//    ***********************************************************************************
//    ***********************************************************************************
//    ***********************************************************************************

    @GetMapping("/transitions")
    Iterable<Transition> getAllTransitions() {
        return service.getAllTransitions();
    }

    @PostMapping("/transitions")
    Transition saveTransition(@RequestBody Transition transition) {
        return service.saveTransition(transition);
    }

    @DeleteMapping("/transitions/{id}")
    void deleteTransition(@PathVariable int id) {
        service.deleteTransition(id);
    }

//    ***********************************************************************************
//    ***********************************************************************************
//    ***********************************************************************************

    @GetMapping("/statuses")
    List<String> getAllStatuses() {
        return service.getAllStatuses();
    }

    @GetMapping("/statuses/{id}")
    List<String> getAllStatusesForIssue(@PathVariable int id) {
        return service.getAllStatusesForIssue(id);
    }

//    ***********************************************************************************
//    ***********************************************************************************
//    ***********************************************************************************

    @PostMapping("/init_default_workflow")
    void initDefaultWorkflow() {
        service.initDefaultWorkflow();
    }



//    ***********************************************************************************
//    ***********************************************************************************
//    ***********************************************************************************

//    ***********************************************************************************
//    ***********************************************************************************
//    ***********************************************************************************

    @PostMapping("/dto/issues")
    Issue createIssue2(@RequestBody IssueCreateDTO issue) {
        return service.saveIssue(convertToEntity(issue));
    }

    @PutMapping("/dto/issues")
    Issue saveIssue2(@RequestBody IssueUpdateDTO issue) {
        return service.saveIssue(convertToEntity(issue));
    }

    private Issue convertToEntity(IssueCreateDTO issueDTO) {
        Issue issue = modelMapper.map(issueDTO, Issue.class);
        issue.setStatus("Open");
        return service.saveIssue(issue);
    }

    private Issue convertToEntity(IssueUpdateDTO issueUpdateDTO) {
        Issue issue = service.getIssue(issueUpdateDTO.getId());
        issue.setUserId(issueUpdateDTO.getUserId());
        //todo: check if status is valid
        issue.setStatus(issueUpdateDTO.getStatus());
        return service.saveIssue(issue);
    }
}
