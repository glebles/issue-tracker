package com.sample.issuetracker.model;

import javax.validation.constraints.NotNull;

public class IssueCreateDTO {
    @NotNull
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
