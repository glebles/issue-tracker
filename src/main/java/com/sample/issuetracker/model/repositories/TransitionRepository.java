package com.sample.issuetracker.model.repositories;

import com.sample.issuetracker.model.entities.Transition;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransitionRepository extends CrudRepository<Transition, Integer> {

    @Cacheable(value = "myCache")
    Iterable<Transition> findAll();

    @CacheEvict(value = "myCache", allEntries = true)
    <S extends Transition> S save(S entity);

    @CacheEvict(value = "myCache", allEntries = true)
    void deleteById(int id);

    @CacheEvict(value = "myCache", allEntries = true)
    void deleteAll();

    @CacheEvict(value = "myCache", allEntries = true)
    <S extends Transition> Iterable<S> saveAll(Iterable<S> entities);
}
