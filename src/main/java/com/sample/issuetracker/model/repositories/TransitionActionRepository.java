package com.sample.issuetracker.model.repositories;

import com.sample.issuetracker.model.entities.Transition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransitionActionRepository extends CrudRepository<Transition, Integer> {
}
