package com.sample.issuetracker.model.repositories;

import com.sample.issuetracker.model.entities.Issue;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueRepository extends CrudRepository<Issue, Integer> {

}
