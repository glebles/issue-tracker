package com.sample.issuetracker.model.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(
        uniqueConstraints=@UniqueConstraint(columnNames={"startStatus","endStatus"})
)
@Entity
public class Transition {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private String startStatus;

    private String endStatus;

    private String title;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "transitionId")
    private List<TransitionAction> transitionActions = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStartStatus() {
        return startStatus;
    }

    public void setStartStatus(String startStatus) {
        this.startStatus = startStatus;
    }

    public String getEndStatus() {
        return endStatus;
    }

    public void setEndStatus(String endStatus) {
        this.endStatus = endStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TransitionAction> getTransitionActions() {
        return transitionActions;
    }

    public void setTransitionActions(List<TransitionAction> transitionActions) {
        this.transitionActions = transitionActions;
    }

    public Transition(String startStatus, String endStatus, String title) {
        this.startStatus = startStatus;
        this.endStatus = endStatus;
        this.title = title;
    }

    public Transition() {
    }
}
