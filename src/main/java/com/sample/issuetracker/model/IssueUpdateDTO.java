package com.sample.issuetracker.model;


import javax.persistence.Id;
import javax.validation.constraints.NotNull;

public class IssueUpdateDTO {
    @Id
    @NotNull
    private int id;

    @NotNull
    private String status;

    @NotNull
    private int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
